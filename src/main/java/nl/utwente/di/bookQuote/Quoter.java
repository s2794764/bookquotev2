package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private HashMap<String, Double> price = new HashMap<>();

    public Quoter() {
        price.put("1", 10.0);
        price.put("2", 45.0);
        price.put("3", 20.0);
        price.put("4", 35.0);
        price.put("5", 50.0);
    }

    public double getBookPrice(String isbn){
        if (price.containsKey(isbn)) {
            return price.get(isbn);
        } else {
            return 0;
        }
    }
}
